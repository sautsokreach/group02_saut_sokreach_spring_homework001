package com.example.homework_1.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;
import java.util.concurrent.atomic.AtomicInteger;

@Data
@NoArgsConstructor
@JsonPropertyOrder(value = {"id", "name", "gender", "age", "address"})
public class CustomerModel{
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int id;
    protected int age;
    private String name,gender,address;
    private static final AtomicInteger count = new AtomicInteger(0);
    public CustomerModel(int age,String name,String gender,String address){
        this.age= age;
        this.name = name;
        this.gender = gender;
        this.address = address;
        this.id = count.incrementAndGet();
    }
    public CustomerModel(CustomerModel insertCustomer){
        this.age= insertCustomer.getAge();
        this.name = insertCustomer.getName();
        this.gender = insertCustomer.getGender();
        this.address = insertCustomer.getAddress();
        this.id = count.incrementAndGet();
    }
    public CustomerModel(int id,CustomerModel updateCustomer){
        this.age= updateCustomer.getAge();
        this.name = updateCustomer.getName();
        this.gender = updateCustomer.getGender();
        this.address = updateCustomer.getAddress();
        this.id = id;
    }

}

