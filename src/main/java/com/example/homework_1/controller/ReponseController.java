package com.example.homework_1.controller;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@JsonPropertyOrder(value = {"message", "customer", "status", "time"})
public class ReponseController<T> {
    @JsonView(View.Public.class)

    String message;
    @JsonView(View.Internal.class)
    T customer;

    @JsonView(View.Public.class)

    String status;
    @JsonView(View.Public.class)

    Timestamp time;
    ReponseController(String message,String status,T data,Timestamp time){
        this.message = message;
        this.status = status;
        this.customer = data;
        this.time = time;
    }
}
