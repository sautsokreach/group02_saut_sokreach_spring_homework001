package com.example.homework_1.controller;
import com.example.homework_1.model.CustomerModel;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class CustomerController {
    List<CustomerModel> AllCustomer = new ArrayList<>();
    public CustomerController() {

        System.out.println("how are you");
        AllCustomer.add(new CustomerModel(18,"Dara","male","Phnomepenh"));
        AllCustomer.add(new CustomerModel(20,"Sokha","male","Batambong"));
        AllCustomer.add(new CustomerModel(32,"Virak","male","Preahvihear"));
        System.out.println("how are you");
    }

    @GetMapping("/api/v1/customers")
    public ResponseEntity<?>  getCustomers() {
        ReponseController<?> ResData = new ReponseController<>("The record has found successfully","Ok",AllCustomer,new Timestamp(System.currentTimeMillis()));
        return new ResponseEntity<>(ResData, HttpStatus.OK);
    }
    @GetMapping("/api/v1/customers/search")
    public ResponseEntity<?>  getCustomersByName(@RequestParam String name) {
        List<CustomerModel> Search = AllCustomer.stream().filter(e->e.getName().equalsIgnoreCase(name)).collect(Collectors.toList());
        ReponseController<?> ResData = new ReponseController<>("This record has found successfully","Ok",Search,new Timestamp(System.currentTimeMillis()));
        return new ResponseEntity<>(Search.isEmpty()?null:ResData, Search.isEmpty()?HttpStatus.NOT_FOUND:HttpStatus.OK);
    }
    @GetMapping("/api/v1/customers/{id}")
    public ResponseEntity<?>  getCustomersById(@PathVariable int id) {
        CustomerModel Search = AllCustomer.stream().filter(e->e.getId()==id).findFirst().orElse(null);
        ReponseController<?> ResData = new ReponseController<>("This record has found successfully","Ok",Search,new Timestamp(System.currentTimeMillis()));
        return new ResponseEntity<>(Search==null?null:ResData, Search==null?HttpStatus.NOT_FOUND:HttpStatus.OK);
    }
    @PostMapping("/api/v1/customers")
    public ResponseEntity<?>  postCustomers(@RequestBody CustomerModel CusInfo) {
        CustomerModel newCustomer = new CustomerModel(CusInfo);
        AllCustomer.add(newCustomer);
        ReponseController<?> ResData = new ReponseController<>("This record was successfully created","Ok",newCustomer,new Timestamp(System.currentTimeMillis()));
        return new ResponseEntity<>(ResData, HttpStatus.CREATED);
    }
    @PutMapping("/api/v1/customers")
    public ResponseEntity<?>  putCustomers(@RequestBody CustomerModel CusInfo,@RequestParam int id) {
        CustomerModel updateCustomer = new CustomerModel(id,CusInfo);
        AllCustomer  = AllCustomer.stream().map(e->e.getId()==id?updateCustomer:e).collect(Collectors.toList());
        long Search  = AllCustomer.stream().filter(e->e.getId()==id).count();
        ReponseController<?> ResData = new ReponseController<>("You're update successfully","Ok",updateCustomer,new Timestamp(System.currentTimeMillis()));
        return new ResponseEntity<>(Search>0?ResData:null, Search>0?HttpStatus.ACCEPTED:HttpStatus.NOT_FOUND);
    }
    @DeleteMapping ("/api/v1/customers")
    @JsonView(View.Public.class)
    public ResponseEntity<?>  deleteCustomers(@RequestParam int id) {
        SimpleFilterProvider filterProvider = new SimpleFilterProvider();
        CustomerModel Search = AllCustomer.stream().filter(e->e.getId()==id).findFirst().orElse(null);
        AllCustomer.removeIf(x -> x.getId()==id);
        ReponseController<?> ResData = new ReponseController<>("Congratulation your delete is successfully","Ok",Search,new Timestamp(System.currentTimeMillis()));
        return new ResponseEntity<>(Search!=null?ResData:null, Search!=null?HttpStatus.ACCEPTED:HttpStatus.NOT_FOUND);
    }

}
